FROM python:3.7-slim
RUN apt-get -q update \
    && apt-get -qy install --no-install-recommends wget \
    && wget -nv -O /tmp/wkhtmltox.deb https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.buster_amd64.deb \
    && apt-get -qy install /tmp/wkhtmltox.deb \
    && rm -rf /var/lib/apt/lists/*

ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt
